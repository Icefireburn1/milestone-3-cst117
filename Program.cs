﻿using System;
using System.Collections.Generic;

// Justin Gewecke

namespace Milestone2_InventoryItem
{
    enum State : int
    {
        openingStatement,
        defaultItemScreen,
        customItemScreen,
        viewItemsScreen,
        exitProgram,
    }

    class Program
    {
        static void Main(string[] args)
        {
            InventoryManager manager = new InventoryManager(100);

            Console.WriteLine("----Welcome to the inventory management system----");
            Console.WriteLine("----WARNING: This program has no checks for invalid input----");


            bool exitProgram = false;
            State screenState = State.openingStatement;
            while (!exitProgram)
            {
                switch (screenState)
                {
                    // Opening Navigation Screen
                    case State.openingStatement:
                        Console.WriteLine("You currently have " + manager.Count() + " items in the list");
                        Console.WriteLine("1. Create an item with default attributes");
                        Console.WriteLine("2. Create an item with custom attributes");
                        Console.WriteLine("3. View/Modify currently created items");
                        Console.WriteLine("4. Exit the program");

                        Console.Write("Enter a number for input: ");
                        string input = Console.ReadLine();
                        screenState = (State)Int32.Parse(input);
                        break;

                    // Screen where we create a default dummy item
                    case State.defaultItemScreen:
                        bool success = manager.AddItem(new InventoryItem());
                        if (success)
                            Console.Write("A placeholder item was added to the system. Press enter to continue.");
                        else
                            Console.Write("The item could not be added. Is the manager full or was it a duplicate item? Use Restock to add more items of the same type.");
                        Console.ReadLine();
                        screenState = State.openingStatement;
                        break;

                    // Screen where we can create a custom item
                    case State.customItemScreen:
                        InventoryItem item = new InventoryItem();
                        
                        Console.Write("Enter item's name: ");
                        item.Name = Console.ReadLine();

                        Console.Write("Enter item's description: ");
                        item.Description = Console.ReadLine();

                        Console.Write("Enter item's selling price: ");
                        item.Cost = float.Parse(Console.ReadLine());

                        Console.Write("Enter item's id: ");
                        item.ID = Int32.Parse(Console.ReadLine());

                        manager.AddItem(item);
                        Console.Write(item.Name + " was added to the system. Press enter to continue.");
                        Console.ReadLine();
                        screenState = State.openingStatement;
                        break;

                    // Screen where we can view and modify objects
                    case State.viewItemsScreen:
                        manager.DisplayItems();

                        Console.WriteLine("1. Remove item");
                        Console.WriteLine("2. Edit item");
                        Console.WriteLine("3. View more details of item");
                        Console.WriteLine("4. Restock an item");
                        Console.WriteLine("5. Go back");
                        Console.WriteLine("To modify an item in the list, type the command #, followed by the item name and the item's cost");
                        Console.WriteLine("(ex: 2 Cheese 1.50 to edit the Cheese item in the list that costs $1.50)");

                        string line = Console.ReadLine();
                        string[] numbers = line.Split(' ');
                        int command = Int32.Parse(numbers[0]);
                        double itemCost = 0;
                        string itemName = "";
                        InventoryItem searchedItem = new InventoryItem();
                        if (numbers.Length > 1)
                        {
                            itemName = numbers[1];
                            itemCost = Double.Parse(numbers[2]);
                            searchedItem = manager.SearchForItem(itemName, itemCost);
                        }

                        // Do different commands depending on input
                        switch(command)
                        {
                            // Remove item from list
                            case 1:
                                Console.WriteLine("Removed " + searchedItem.Name + " from the system. Press enter to continue.");
                                manager.RemoveItem(searchedItem);
                                break;

                            // Modify item in list
                            case 2:
                                Console.Clear();
                                InventoryItem tempItem = searchedItem;
                                Console.WriteLine("---Item you are modifying---");
                                Console.WriteLine("Name: " + tempItem.Name);
                                Console.WriteLine("Description: " + tempItem.Description);
                                Console.WriteLine("Cost: " + tempItem.Cost);
                                Console.WriteLine("Item ID: " + tempItem.ID);
                                Console.WriteLine("OnFloor: " + tempItem.OnFloor);
                                Console.WriteLine("InBackroom: " + tempItem.InBackroom);
                                Console.WriteLine("Discontinued: " + tempItem.IsDiscontinued);
                                Console.WriteLine("----------------------------");
                                Console.WriteLine("Edit an item's attributes. Leave input empty to keep current attribute unchanged");
                                Console.Write("Enter item's name: ");
                                tempItem.Name = Console.ReadLine();

                                Console.Write("Enter item's description: ");
                                tempItem.Description = Console.ReadLine();

                                Console.Write("Enter item's selling price: ");
                                tempItem.Cost = float.Parse(Console.ReadLine());

                                Console.Write("Enter item's id: ");
                                tempItem.ID = Int32.Parse(Console.ReadLine());

                                Console.Write("Enter onFloor count: ");
                                tempItem.OnFloor = Int32.Parse(Console.ReadLine());

                                Console.Write("Enter inBackroom count: ");
                                tempItem.InBackroom = Int32.Parse(Console.ReadLine());

                                Console.Write("Enter is it discontinued? (true/false): ");
                                tempItem.IsDiscontinued = Boolean.Parse(Console.ReadLine());

                                manager.RemoveItem(searchedItem);
                                manager.AddItem(tempItem);
                                Console.WriteLine("The item was modified. Press enter to continue.");
                                break;

                            // View details of item in list
                            case 3:
                                Console.Clear();
                                InventoryItem _tempItem = searchedItem;
                                Console.WriteLine("---Item you are modifying---");
                                Console.WriteLine("Name: " + _tempItem.Name);
                                Console.WriteLine("Description: " + _tempItem.Description);
                                Console.WriteLine("Cost: " + _tempItem.Cost);
                                Console.WriteLine("Item ID: " + _tempItem.ID);
                                Console.WriteLine("OnFloor: " + _tempItem.OnFloor);
                                Console.WriteLine("InBackroom: " + _tempItem.InBackroom);
                                Console.WriteLine("Discontinued: " + _tempItem.IsDiscontinued);
                                Console.WriteLine("----------------------------");
                                Console.WriteLine("Press enter to return to previous screen.");
                                break;

                            // Restock an item
                            case 4:
                                Console.Clear();
                                Console.Write("How many are you stocking?: ");
                                string answer = Console.ReadLine();
                                int num = Int32.Parse(answer);
                                manager.RestockItem(searchedItem, num);
                                Console.WriteLine("The item now has " + searchedItem.OnFloor + " items on the floor. Press enter to continue.");
                                break;
                            // Returns us to opening screen
                            case 5:
                                screenState = State.openingStatement;
                                Console.Clear();
                                continue;
                        }
                        Console.ReadLine();
                        break;

                    // Screen that exits the program for us
                    case State.exitProgram:
                        exitProgram = true;
                        break;

                }
                Console.Clear();
            }
            Console.WriteLine("Thank you for using my program!");
        }
    }
}
