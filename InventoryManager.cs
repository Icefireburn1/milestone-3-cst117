﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Milestone2_InventoryItem
{
    class InventoryManager
    {
        InventoryItem[] itemList;

        // Return size of itemList array
        public int MaxCount
        {
            get { return itemList.Length; }
        }

        public InventoryManager(int inventoryManagerSize)
        {
            itemList = new InventoryItem[inventoryManagerSize];
        }

        // Add item to array list but not if it exists already
        public bool AddItem(InventoryItem item)
        {
            // Check for item duplicates
            foreach(InventoryItem i in itemList)
            {
                if (i != null)
                {
                    if (i.Name.ToLower() == item.Name.ToLower())
                        return false;
                }
            }
            // Check for empty spot
            for (int i = 0; i < itemList.Length; i++)
            {
                if (itemList[i] == null)
                {
                    itemList[i] = item;
                    return true;
                }
            }
            return false;   // We could not add item (was there no room?)
        }

        // Remove an item from the list if it exists
        public bool RemoveItem(InventoryItem item)
        {
            int temp = ItemExists(item);
            if (temp != -1)
            {
                itemList[temp] = null;
                return true;
            }
            return false;
        }

        // Change an items onFloor count if the item exists
        public bool RestockItem(InventoryItem item, int number)
        {
            int temp = ItemExists(item);
            if (temp != -1)
            {
                itemList[temp].OnFloor += number;
                return true;
            }
            return false;
        }

        // Display the items in our list
        public void DisplayItems()
        {
            Console.WriteLine("----Items in the InventoryManager----");
            for (int i = 0; i < itemList.Length; i++)
            {
                if (itemList[i] != null)
                {
                    Console.WriteLine(itemList[i].Name + " - Cost: $" + itemList[i].Cost + " Quantity: " + itemList[i].OnFloor);
                }
            }
            Console.WriteLine("-------------------------------------");
        }

        // Return an item give its name and price
        public InventoryItem SearchForItem(string name, double price)
        {
            for (int i = 0; i < itemList.Length; i++)
            {
                if (itemList[i].Name.ToLower() == name.ToLower())
                {
                    if (itemList[i].Cost == price)
                    {
                        return itemList[i];
                    }
                }
            }
            return null;
        }

        // Check if an item exists in the manager, return index if true
        private int ItemExists(InventoryItem item)
        {
            for (int i = 0; i < itemList.Length; i++)
            {
                if (itemList[i] == item)
                {
                    return i;
                }
            }
            return -1;   // We could not find the item to remove
        }

        // Return the number of items in the list that aren't null
        public int Count()
        {
            int num = 0;
            for (int i = 0; i < itemList.Length; i++)
            {
                if (itemList[i] != null)
                    num++;
            }
            return num;
        }
    }
}
